package com.hakuapps.streamingtest.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.hakuapps.streamingtest.R;
import com.hakuapps.streamingtest.BroadcastActivity;
import com.hakuapps.streamingtest.models.WowConfig;

public class BroadcastFragment extends Fragment {

    public static final String CONFIG = "config";

    TextInputLayout hostlayout;
    TextInputEditText hostET;
    TextInputLayout portlayout;
    TextInputEditText porttET;
    TextInputLayout applicationlayout;
    TextInputEditText applicationET;
    TextInputLayout streamlayout;
    TextInputEditText streamET;
    TextInputLayout userNamelayout;
    TextInputEditText userNameET;
    TextInputLayout passwordlayout;
    TextInputEditText passwordNameET;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_broadcast, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        hostlayout = view.findViewById(R.id.host_layout);
        hostET = view.findViewById(R.id.host_edittext);
        portlayout = view.findViewById(R.id.port_layout);
        porttET = view.findViewById(R.id.port_edittext);
        applicationlayout = view.findViewById(R.id.application_layout);
        applicationET = view.findViewById(R.id.applicaiton_editttext);
        streamlayout = view.findViewById(R.id.stream_layout);
        streamET = view.findViewById(R.id.stream_edittext);
        userNamelayout = view.findViewById(R.id.username_layout);
        userNameET = view.findViewById(R.id.username_edittext);
        passwordlayout = view.findViewById(R.id.password_layout);
        passwordNameET = view.findViewById(R.id.password_edittext);

        Button broadcastButton = view.findViewById(R.id.broadcast_button);
        broadcastButton.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), BroadcastActivity.class);
            WowConfig config = new WowConfig(
                    hostET.getText().toString(),
                    Integer.valueOf(porttET.getText().toString()),
                    applicationET.getText().toString(),
                    streamET.getText().toString(),
                    userNameET.getText().toString(),
                    passwordNameET.getText().toString()
            );
            intent.putExtra(CONFIG, config);
            startActivity(intent);
        });
    }
}
