package com.hakuapps.streamingtest.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hakuapps.streamingtest.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MediaFragment extends Fragment {

    private EditText mEdtText;
    private Button mGalapagosButton;
    private Button mInternetButton;
    private Button mCartoonButton;
    private Button mStremingButton;
    private ImageButton mPlayButton;
    private VideoView mVideoView;
    private boolean isPlaying;
    public MediaFragment() {}
    private ProgressBar mProgressBar;
    private static final String[] DEMOSURLS = {
            "https://firebasestorage.googleapis.com/v0/b/plusone-e40db.appspot.com/o/Galapagos%202018%20part%201%20-%20introduction.mp4?alt=media&token=f5f59a48-8b28-49a5-9e6d-20a42d22adb5",
            "https://ia800201.us.archive.org/22/items/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4",
            "http://www.onirikal.com/videos/mp4/assembly_line.mp4",
            "rtsp://192.168.100.34:1935/live/myStream"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_media, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mVideoView = view.findViewById(R.id.video_view);
        mProgressBar = view.findViewById(R.id.progress_circular);
        mEdtText = view.findViewById(R.id.edittext);
        mEdtText.setText(DEMOSURLS[1]);
        mEdtText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                mVideoView.stopPlayback();
                isPlaying = false;
                mPlayButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);}
        });
        mGalapagosButton = view.findViewById(R.id.galapagos_button);
        mInternetButton = view.findViewById(R.id.internet_button);
        mCartoonButton = view.findViewById(R.id.cartoon_button);
        mStremingButton = view.findViewById(R.id.stream_button);
        mGalapagosButton.setOnClickListener(v -> {
            mEdtText.setText(DEMOSURLS[0]);
            tryToPlayVideo();
            setClipboard(getContext(), DEMOSURLS[0]);
        });
        mInternetButton.setOnClickListener(v -> {
            mEdtText.setText(DEMOSURLS[1]);
            tryToPlayVideo();
            setClipboard(getContext(), DEMOSURLS[1]);
        });
        mCartoonButton.setOnClickListener(v -> {
            mEdtText.setText(DEMOSURLS[2]);
            tryToPlayVideo();
            setClipboard(getContext(), DEMOSURLS[2]);
        });
        mStremingButton.setOnClickListener(v -> {
            mEdtText.setText(DEMOSURLS[3]);
            new AlertDialog.Builder(getContext())
                    .setTitle("Info")
                    .setMessage("Before to do this. please Config the Wowza Streaming Engine on your PC. Otherwise it will not work.\nRemember check the host ip ;)")
                    .setPositiveButton("OK", (dialog, which) -> tryToPlayVideo())
                    .show();
            setClipboard(getContext(), DEMOSURLS[3]);
        });


        mPlayButton = view.findViewById(R.id.play_button);
        mPlayButton.setOnClickListener(v -> {
            if (mVideoView.isPlaying()){
                mVideoView.pause();
                mPlayButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
            }else {
                tryToPlayVideo();
            }
        });

        tryToPlayVideo();
    }

    private void tryToPlayVideo(){
        mProgressBar.setVisibility(View.VISIBLE);
        if (isPlaying){
            mVideoView.start();
            mPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
            mProgressBar.setVisibility(View.GONE);
            return;
        }

        try{
            mVideoView.setVideoURI(Uri.parse(mEdtText.getText().toString()));
            mVideoView.setOnPreparedListener(mp -> {
                mVideoView.requestFocus();
                mPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
                mVideoView.start();
                mProgressBar.setVisibility(View.GONE);
                isPlaying = true;
            });
        }catch (Exception e){
            new AlertDialog.Builder(getContext())
                    .setMessage(e.toString())
                    .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                    .show();
        }
    }

    private void setClipboard(Context context, String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied URL to Clipboard", text);
            clipboard.setPrimaryClip(clip);
        }
    }

}
