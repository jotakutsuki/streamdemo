package com.hakuapps.streamingtest.models;

import java.io.Serializable;

public class WowConfig implements Serializable {

    private String host;
    private int port;
    private String applicationName;
    private String streamName;
    private String userName;
    private String password;

    public WowConfig(String host, int port, String applicationName, String streamName, String userName, String password) {
        this.host = host;
        this.port = port;
        this.applicationName = applicationName;
        this.streamName = streamName;
        this.userName = userName;
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
